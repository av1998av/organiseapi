exports.userSigninValidator = (req,res,next) => {
    req.check("uname", "Uame is required").notEmpty();
    req.check("pwd", "Password is required").notEmpty();
    const errors = req.validationErrors()
    if (errors){
        const firstError = errors.map(error => error.msg)[0]
        return res.status(400).json({error: firstError})
    }
    next();
}

exports.addCalendarValidator = (req,res,next) => {
    req.check("date", "Date is required").notEmpty();
    req.check("dept", "Department is required").notEmpty();
    req.check("desc", "Description is required").notEmpty();
    const errors = req.validationErrors()
    if (errors){
        const firstError = errors.map(error => error.msg)[0]
        return res.status(400).json({error: firstError})
    }
    next();
}

exports.addInventoryValidator = (req,res,next) => {
    req.check("name", "Name is required").notEmpty();
    const errors = req.validationErrors()
    if (errors){
        const firstError = errors.map(error => error.msg)[0]
        return res.status(400).json({error: firstError})
    }
    next();
}
