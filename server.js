const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const cors = require('cors');
const dotenv = require('dotenv');
const expressValidator = require('express-validator') 
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const userRoutes = require('./routes/user_routes');
const calendarRoutes = require('./routes/calendar_routes');
const inventoryRoutes = require('./routes/inventory_routes');


//app
const app = express();

//config variables 
dotenv.config();

//port
const port = process.env.PORT;

//body parser 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//cors 
app.use(cors());

//validator
app.use(expressValidator());

//api routes
app.use("/calendar", calendarRoutes);
app.use("/inventory", inventoryRoutes);
app.use("/admin", userRoutes);

//documentation route
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(port, '172.31.16.36', () =>{
//app.listen(port, () =>{
    console.log(`Server runnning on port ${port}`);
});
