'use strict';
module.exports = (sequelize, DataTypes) => {
  const Calendar = sequelize.define('Calendar', {
    date: DataTypes.STRING,
    desc: DataTypes.STRING,
    dept: DataTypes.STRING
  }, {});
  Calendar.associate = function(models) {
    // associations can be defined here
  };
  return Calendar;
};