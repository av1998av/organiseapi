'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    uname: DataTypes.STRING,
    hashed_pwd: DataTypes.STRING,
    salt: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};