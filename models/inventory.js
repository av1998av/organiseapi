'use strict';
module.exports = (sequelize, DataTypes) => {
  const Inventory = sequelize.define('Inventory', {
    name: DataTypes.STRING,
    cost: DataTypes.INTEGER,
    qty: DataTypes.INTEGER,
    desc: DataTypes.STRING
  }, {});
  Inventory.associate = function(models) {
    // associations can be defined here
  };
  return Inventory;
};