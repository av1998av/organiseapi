const Calendar = require('../models').Calendar;

exports.addCalendar = (req, res) =>{
    console.log(Calendar);
    Calendar.create({
        date: req.body.date,
        desc: req.body.desc,
        dept: req.body.dept
    }).then(function(Calendar){
        if(Calendar){
            return res.status(200).send({message: 'Event added'});
        }
        else{
            return res.status(400).send({message: 'Server error'});
        }
    });
};

exports.listCalendar = (req, res) => {
    Calendar.findAll().then(function(calendars){
        if(calendars){
            return res.status(200).send({message: 'Success', Calendars: calendars});
        }   
        else{
            return res.status(400).send({message: 'Events not found'});
        }
    });
};