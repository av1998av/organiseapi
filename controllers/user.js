const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

const User = require('../models').User;

dotenv.config();


exports.signin = (req, res) =>{
    User.findOne({ where: {uname:req.body.uname}}).then(function(User){
        if(User){
            new_hashed_pwd = crypto.createHash('sha1', User.salt).update(req.body.pwd).digest('hex');
            if(new_hashed_pwd === User.hashed_pwd){
                const token = jwt.sign({id:User.id}, process.env.JWT_ENCRYPTION, {expiresIn: parseInt(process.env.JWT_EXPIRATION)});
                return res.status(200).send({message: 'User Signed in', kid: User.id, token: token});
            }
            else{
                return res.status(400).send({message: 'Wrong credentials'});
            }
        }
        else{
            return res.status(401).send({message: 'User not found'});
        }
    });
};

