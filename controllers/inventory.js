const Inventory = require('../models').Inventory;

exports.addInventory = (req, res) =>{
    Inventory.create({
         name: req.body.name,
         desc: req.body.desc,
         cost: req.body.cost,
	 qty: req.body.qty
    }).then(function(Inventory){
        if(Inventory){
            return res.status(200).send({message: 'Product added'});
        }
        else{
            return res.status(400).send({message: 'Server error'});
        }
    });
};

exports.listInventory = (req, res) => {
    Inventory.findAll().then(function(products){
        if(products){
            return res.status(200).send({message: 'Success', Products: products});
        }   
        else{
            return res.status(400).send({message: 'Events not found'});
        }
    });
};
