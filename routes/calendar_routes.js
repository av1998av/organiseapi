const express = require('express')
const router = express.Router();

const Auth = require('../controllers/auth')
const Calendar = require('../controllers/calendar')
const Validator = require('../validator/index');

router.post('/admin/addCalendar', Auth.checkAuthUser, Validator.addCalendarValidator,  Calendar.addCalendar);
router.get('/listCalendar',Calendar.listCalendar);

module.exports = router;

