const express = require('express')
const router = express.Router();

const Auth = require('../controllers/auth')
const Inventory = require('../controllers/inventory')
const Validator = require('../validator/index');

router.post('/admin/addInventory', Auth.checkAuthUser, Validator.addInventoryValidator, Inventory.addInventory);
router.get('/admin/listInventory', Auth.checkAuthUser, Inventory.listInventory);

module.exports = router;

