const express = require('express');
const router = express.Router();

const User = require('../controllers/user');
const Validator = require('../validator/index');

router.post('/login', Validator.userSigninValidator, User.signin);


module.exports = router;
